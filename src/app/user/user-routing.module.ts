import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';

const routes = [
  {path: '', component: UserListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
